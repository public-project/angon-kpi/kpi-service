/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.UserRole;

/**
 * @author aulia
 *
 */
public interface UserRoleService {

	public UserRole doSaveUser(UserRole userRole);

	public void doDeleteUser(Long id);

	public Page<UserRole> getAllUserRole(int page, int size, String keyword);

	public UserRole getUserRole(Long id);

}
