/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.UserManagement;

/**
 * @author aulia
 *
 */
public interface UserService {

	public UserManagement doSaveUser(UserManagement user);

	public void doDeleteUser(Long id);

	public Page<UserManagement> getAllUser(int page, int size, String keyword);

	public UserManagement getUser(Long id);

	public UserManagement getUserByUserName(String userName);

}
