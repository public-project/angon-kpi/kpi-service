/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.ParticipantClass;

/**
 * @author aulia
 *
 */
public interface ParticipantClassService {

	public ParticipantClass doSaveParticipantClass(ParticipantClass participantClass);

	public void doDeleteParticipantClass(Long id);

	public Page<ParticipantClass> getAllParticipantClass(int page, int size, String keyword);

	public ParticipantClass getParticipantClass(Long id);

}
