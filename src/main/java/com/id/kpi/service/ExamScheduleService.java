/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.ExamSchedule;

/**
 * @author aulia
 *
 */
public interface ExamScheduleService {

	public ExamSchedule doSaveExamSchedule(ExamSchedule examSchedule);

	public void doDeleteExamSchedule(Long id);

	public Page<ExamSchedule> getAllExamSchedule(int page, int size, String keyword);

	public ExamSchedule getExamSchedule(Long id);

}
