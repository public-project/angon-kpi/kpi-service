/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.Participant;

/**
 * @author aulia
 *
 */
public interface ParticipantService {

	public Participant doSaveParticipant(Participant participant);

	public void doDeleteParticipant(Long id);

	public Page<Participant> getAllParticipant(int page, int size, String keyword);

	public Participant getParticipant(Long id);
}
