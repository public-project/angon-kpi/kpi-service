/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.QuestionDetail;

/**
 * @author aulia
 *
 */
public interface QuestionDetailService {

	public QuestionDetail doSaveQuestionDetail(QuestionDetail questionDetail);

	public void doDeleteQuestionDetail(Long id);

	public Page<QuestionDetail> getAllQuestionDetail(int page, int size);

	public QuestionDetail getQuestionDetail(Long id);

}
