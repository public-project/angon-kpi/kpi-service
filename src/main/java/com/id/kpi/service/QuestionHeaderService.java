/**
 * 
 */
package com.id.kpi.service;

import org.springframework.data.domain.Page;

import com.id.kpi.entity.QuestionHeader;

/**
 * @author aulia
 *
 */
public interface QuestionHeaderService {

	public QuestionHeader doSaveQuestionHeader(QuestionHeader questionHeader);

	public void doDeleteQuestionHeader(Long id);

	public Page<QuestionHeader> getAllQuestionHeader(int page, int size, String keyword);

	public QuestionHeader getQuestionHeader(Long id);

}
