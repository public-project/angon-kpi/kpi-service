/**
 * 
 */
package com.id.kpi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.QuestionDetail;
import com.id.kpi.repository.QuestionDetailRepository;
import com.id.kpi.service.QuestionDetailService;

/**
 * @author aulia
 *
 */
@Service
public class QuestionDetailServiceImpl extends BaseService implements QuestionDetailService {
	
	@Autowired
	private QuestionDetailRepository questionDetailRepository;

	@Override
	public QuestionDetail doSaveQuestionDetail(QuestionDetail questionDetail) {
		return questionDetailRepository.save(questionDetail);
	}

	@Override
	public void doDeleteQuestionDetail(Long id) {
		questionDetailRepository.delete(id);		
	}

	@Override
	public Page<QuestionDetail> getAllQuestionDetail(int page, int size) {
		return questionDetailRepository.findAll(createPageRequest(page, size));
	}

	@Override
	public QuestionDetail getQuestionDetail(Long id) {
		return questionDetailRepository.findOne(id);
	}

}
