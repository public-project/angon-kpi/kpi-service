/**
 * 
 */
package com.id.kpi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.ExamSchedule;
import com.id.kpi.repository.ExamScheduleRepository;
import com.id.kpi.service.ExamScheduleService;

/**
 * @author aulia
 *
 */
@Service
public class ExamScheduleServiceImpl extends BaseService implements ExamScheduleService {

	@Autowired
	private ExamScheduleRepository examScheduleRepository;

	@Override
	public ExamSchedule doSaveExamSchedule(ExamSchedule examSchedule) {
		return examScheduleRepository.save(examSchedule);
	}

	@Override
	public void doDeleteExamSchedule(Long id) {
		examScheduleRepository.delete(id);
	}

	@Override
	public Page<ExamSchedule> getAllExamSchedule(int page, int size, String keyword) {
		return examScheduleRepository.findAll(createPageRequest(page, size));
	}

	@Override
	public ExamSchedule getExamSchedule(Long id) {
		return examScheduleRepository.findOne(id);
	}

}
