/**
 * 
 */
package com.id.kpi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.UserRole;
import com.id.kpi.repository.UserRoleRepository;
import com.id.kpi.service.UserRoleService;

/**
 * @author aulia
 *
 */
@Service
public class UserRoleServiceImpl extends BaseService implements UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Override
	public UserRole doSaveUser(UserRole userRole) {
		return userRoleRepository.save(userRole);
	}

	@Override
	public void doDeleteUser(Long id) {
		userRoleRepository.delete(id);
	}

	@Override
	public Page<UserRole> getAllUserRole(int page, int size, String keyword) {
		return userRoleRepository.findByRoleNameContaining(createPageRequest(page, size), keyword);
	}

	@Override
	public UserRole getUserRole(Long id) {
		return userRoleRepository.findOne(id);
	}

}
