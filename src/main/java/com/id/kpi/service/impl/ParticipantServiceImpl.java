/**
 * 
 */
package com.id.kpi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.Participant;
import com.id.kpi.repository.ParticipantRepository;
import com.id.kpi.service.ParticipantService;

/**
 * @author aulia
 *
 */
@Service
public class ParticipantServiceImpl extends BaseService implements ParticipantService {

	@Autowired
	private ParticipantRepository participantRepository;

	@Override
	public Participant doSaveParticipant(Participant participant) {
		return participantRepository.save(participant);
	}

	@Override
	public void doDeleteParticipant(Long id) {
		participantRepository.delete(id);
	}

	@Override
	public Page<Participant> getAllParticipant(int page, int size, String keyword) {
		return participantRepository.findByParticipantFullNameContaining(createPageRequest(page, size), keyword);
	}

	@Override
	public Participant getParticipant(Long id) {
		return participantRepository.findOne(id);
	}

}
