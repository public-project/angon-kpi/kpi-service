/**
 * 
 */
package com.id.kpi.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.id.kpi.entity.Participant;

/**
 * @author aulia
 *
 */
@Service
public class BulkParticipantService extends SimpleJpaRepository<Participant, Long> {

	private EntityManager entityManager;

	public BulkParticipantService(EntityManager entityManager) {
		super(Participant.class, entityManager);
		this.entityManager = entityManager;
	}

	@Transactional
	public List<Participant> save(List<Participant> participants) {
		participants.forEach(participant -> entityManager.persist(participant));
		return participants;
	}
}
