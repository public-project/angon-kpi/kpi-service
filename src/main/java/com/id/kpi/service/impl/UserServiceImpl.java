/**
 * 
 */
package com.id.kpi.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.UserManagement;
import com.id.kpi.repository.UserManagementRepository;
import com.id.kpi.service.UserService;

/**
 * @author aulia
 *
 */
@Service
public class UserServiceImpl extends BaseService implements UserService, UserDetailsService {

	@Autowired
	private UserManagementRepository userManagementRepository;

	@Override
	public UserManagement doSaveUser(UserManagement user) {
		return userManagementRepository.save(user);
	}

	@Override
	public void doDeleteUser(Long id) {
		userManagementRepository.delete(id);
	}

	@Override
	public Page<UserManagement> getAllUser(int page, int size, String keyword) {
		return userManagementRepository.findByUserNameContaining(createPageRequest(page, size), keyword);
	}

	@Override
	public UserManagement getUser(Long id) {
		return userManagementRepository.findOne(id);
	}

	@Override
	public UserManagement getUserByUserName(String userName) {
		return userManagementRepository.findByUserName(userName);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserManagement user = userManagementRepository.findByUserName(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getUserPassword(),
				getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}

}
