/**
 * 
 */
package com.id.kpi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.QuestionHeader;
import com.id.kpi.repository.QuestionHeaderRepository;
import com.id.kpi.service.QuestionHeaderService;

/**
 * @author aulia
 *
 */
@Service
public class QuestionHeaderServiceImpl extends BaseService implements QuestionHeaderService {
	
	@Autowired
	private QuestionHeaderRepository questionHeaderRepository;

	@Override
	public QuestionHeader doSaveQuestionHeader(QuestionHeader questionHeader) {
		return questionHeaderRepository.save(questionHeader);
	}

	@Override
	public void doDeleteQuestionHeader(Long id) {
		questionHeaderRepository.delete(id);		
	}

	@Override
	public Page<QuestionHeader> getAllQuestionHeader(int page, int size, String keyword) {
		return questionHeaderRepository.findByQuestionCodeContaining(createPageRequest(page, size), keyword);
	}

	@Override
	public QuestionHeader getQuestionHeader(Long id) {
		return questionHeaderRepository.findOne(id);
	}

}
