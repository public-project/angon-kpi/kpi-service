/**
 * 
 */
package com.id.kpi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.id.kpi.base.BaseService;
import com.id.kpi.entity.ParticipantClass;
import com.id.kpi.repository.ParticipantClassRepository;
import com.id.kpi.service.ParticipantClassService;

/**
 * @author aulia
 *
 */
@Service
public class ParticipantClassServiceImpl extends BaseService implements ParticipantClassService {
	
	@Autowired
	private ParticipantClassRepository participantClassRepository;

	@Override
	public ParticipantClass doSaveParticipantClass(ParticipantClass participantClass) {
		return participantClassRepository.save(participantClass);
	}

	@Override
	public void doDeleteParticipantClass(Long id) {
		participantClassRepository.delete(id);		
	}

	@Override
	public Page<ParticipantClass> getAllParticipantClass(int page, int size, String keyword) {
		return participantClassRepository.findByClassNameContaining(createPageRequest(page, size), keyword);
	}

	@Override
	public ParticipantClass getParticipantClass(Long id) {
		return participantClassRepository.findOne(id);
	}

}
