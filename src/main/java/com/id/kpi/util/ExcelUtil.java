/**
 * 
 */
package com.id.kpi.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.id.kpi.constant.KpiConstant;
import com.id.kpi.entity.Participant;
import com.id.kpi.entity.ParticipantClass;
import com.id.kpi.service.impl.BulkParticipantService;

/**
 * @author aulia
 *
 */
@Component
public class ExcelUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil.class);

	@Autowired
	private BulkParticipantService bulkParticipantService;

	public String processParticipantFile(Path pathFile) {
		Workbook workbook = null;
		try (FileInputStream fileStream = new FileInputStream(pathFile.toFile())) {
			String fileName = pathFile.getFileName().toString();

			workbook = getWorkbook(fileStream, fileName);
			Sheet sheet = workbook.getSheet(KpiConstant.TEMPLATE_PARTICIPANT_XLS_WORKSHEET);
			Iterator<Row> iterator = sheet.iterator();

			List<Participant> listOfParticipant = new ArrayList<>();
			while (iterator.hasNext()) {
				Participant participant = new Participant();
				Row currentRow = iterator.next();
				if (currentRow.getRowNum() > 0) {
					ParticipantClass participantClass = new ParticipantClass();
					participantClass.setId((long) currentRow.getCell(2).getNumericCellValue());
					participant.setParticipantCode(currentRow.getCell(0).getStringCellValue());
					participant.setParticipantFullName(currentRow.getCell(1).getStringCellValue());
					participant.setParticipantClass(participantClass);
					listOfParticipant.add(participant);
				}
			}
			bulkParticipantService.save(listOfParticipant);
			return KpiConstant.SUCCESS;
		} catch (IOException e) {
			LOGGER.error("process excel", e);
			return KpiConstant.FAILED;
		} finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e) {
					LOGGER.error("failed to close workbook", e);
				}
			}
		}
	}

	private Workbook getWorkbook(FileInputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;
		if (excelFilePath.endsWith(KpiConstant.EXCEL_XLS)) {
			workbook = new HSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith(KpiConstant.EXCEL_XLSX)) {
			workbook = new XSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("Incorrect file format");
		}
		return workbook;
	}

}
