/**
 * 
 */
package com.id.kpi.base;

import org.springframework.data.domain.PageRequest;

/**
 * @author aulia
 *
 */
public abstract class BaseService {

	protected PageRequest createPageRequest(int page, int size) {
		return new PageRequest(page, size);
	}
}
