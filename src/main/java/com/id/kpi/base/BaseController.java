/**
 * 
 */
package com.id.kpi.base;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author aulia
 *
 */
@RequestMapping(value = "/api", produces = "application/json")
public class BaseController {

	protected String checkIsNull(String words) {
		if (StringUtils.isEmpty(words)) {
			return "";
		}
		return words;
	}

}
