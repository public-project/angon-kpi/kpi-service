/**
 * 
 */
package com.id.kpi.base.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author aulia
 *
 */
public class KpiResponseWrapper {

	@JsonInclude(value = Include.NON_NULL)
	private String responseMessage;
	
	@JsonInclude(value = Include.NON_NULL)
	private Object data;

	/**
	 * Default constructor
	 */
	public KpiResponseWrapper() {
		super();
	}

	/**
	 * @param responseMessage
	 * @param data
	 */
	public KpiResponseWrapper(String responseMessage, Object data) {
		this.responseMessage = responseMessage;
		this.data = data;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage
	 *            the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

}
