/**
 * 
 */
package com.id.kpi.base.exception;

/**
 * @author aulia
 *
 */
public class KpiException extends Exception {

	private static final long serialVersionUID = 1797558461468356944L;

	/**
	 * Default constructor
	 */
	public KpiException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public KpiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public KpiException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public KpiException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public KpiException(Throwable cause) {
		super(cause);
	}

}
