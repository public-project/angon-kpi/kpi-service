/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.ExamSchedule;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "Exam Schedule API")
public interface ExamScheduleController {

	@ApiOperation(value = "Save Exam Schedule")
	public ResponseEntity<KpiResponseWrapper> doSaveExamSchedule(ExamSchedule examSchedule) throws KpiException;

	@ApiOperation(value = "Update Exam Schedule")
	public ResponseEntity<KpiResponseWrapper> doUpdateExamSchedule(Long id, ExamSchedule examSchedule)
			throws KpiException;

	@ApiOperation(value = "Delete Exam Schedule")
	public ResponseEntity<KpiResponseWrapper> doDeleteExamSchedule(Long id) throws KpiException;

	@ApiOperation(value = "Get All Exam Schedule")
	public ResponseEntity<KpiResponseWrapper> getAllExamSchedule(int page, int size, String keyword)
			throws KpiException;

	@ApiOperation(value = "Get Exam Schedule")
	public ResponseEntity<KpiResponseWrapper> getExamSchedule(Long id) throws KpiException;

}
