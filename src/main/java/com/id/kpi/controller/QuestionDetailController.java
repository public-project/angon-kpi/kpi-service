/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.QuestionDetail;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "Question Detail API")
public interface QuestionDetailController {

	@ApiOperation(value = "Save Question Detail")
	public ResponseEntity<KpiResponseWrapper> doSaveQuestionDetail(QuestionDetail questionDetail) throws KpiException;

	@ApiOperation(value = "Update Question Detail")
	public ResponseEntity<KpiResponseWrapper> doUpdateQuestionDetail(Long id, QuestionDetail questionDetail)
			throws KpiException;

	@ApiOperation(value = "Delete Question Detail")
	public ResponseEntity<KpiResponseWrapper> doDeleteQuestionDetail(Long id) throws KpiException;

	@ApiOperation(value = "Get All Question Detail")
	public ResponseEntity<KpiResponseWrapper> getAllQuestionDetail(int page, int size) throws KpiException;

	@ApiOperation(value = "Get Question Detail")
	public ResponseEntity<KpiResponseWrapper> getQuestionDetail(Long id) throws KpiException;
	
	@ApiOperation(value = "Get Question Detail By Question Header ID")
	public ResponseEntity<KpiResponseWrapper> getQuestionDetailByHeader(int page, int size, Long headerId) throws KpiException;

}
