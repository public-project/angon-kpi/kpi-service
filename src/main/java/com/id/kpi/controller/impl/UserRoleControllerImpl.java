/**
 * 
 */
package com.id.kpi.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.UserRoleController;
import com.id.kpi.entity.UserRole;
import com.id.kpi.service.UserRoleService;

/**
 * @author aulia
 *
 */
@RestController
public class UserRoleControllerImpl extends BaseController implements UserRoleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRoleControllerImpl.class);

	@Autowired
	private UserRoleService userRoleService;

	@Override
	@PostMapping(value = "role")
	public ResponseEntity<KpiResponseWrapper> doSaveUserRole(@RequestBody UserRole role) throws KpiException {
		try {
			UserRole responseBody = userRoleService.doSaveUser(role);
			return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.error("error on save user role", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "role/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateUserRole(@PathVariable(value = "id") Long id,
			@RequestBody UserRole role) throws KpiException {
		try {
			UserRole existingRole = userRoleService.getUserRole(id);
			if (existingRole != null) {
				UserRole responseBody = userRoleService.doSaveUser(role);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			}
			return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.ROLE_NOT_FOUND, null), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			LOGGER.error("error on update user role", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "role/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteUserRole(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			UserRole existingRole = userRoleService.getUserRole(id);
			if (existingRole != null) {
				userRoleService.doDeleteUser(existingRole.getId());
				return new ResponseEntity<>(new KpiResponseWrapper(), HttpStatus.OK);
			}
			return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.ROLE_NOT_FOUND, null), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			LOGGER.error("error on delete user role", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "role")
	public ResponseEntity<KpiResponseWrapper> getAllUserRole(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "keyword", required = false) String keyword)
			throws KpiException {
		try {
			Page<UserRole> responseBody = userRoleService.getAllUserRole(page, size, checkIsNull(keyword));
			return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("error on list user role", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "role/{id}")
	public ResponseEntity<KpiResponseWrapper> getUserRole(@PathVariable(value = "id") Long id) throws KpiException {
		try {
			UserRole responseBody = userRoleService.getUserRole(id);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			}
			return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.ROLE_NOT_FOUND, null), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			LOGGER.error("error on get user role", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
