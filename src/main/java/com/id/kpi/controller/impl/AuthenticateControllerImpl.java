/**
 * 
 */
package com.id.kpi.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.controller.AuthenticateController;
import com.id.kpi.entity.UserManagement;
import com.id.kpi.service.UserService;
import com.id.kpi.util.JwtTokenUtil;

/**
 * @author aulia
 *
 */
@RestController
public class AuthenticateControllerImpl extends BaseController implements AuthenticateController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService userManagementService;

	@Override
	@PostMapping(value = "signin")
	public ResponseEntity<KpiResponseWrapper> doSignIn(@RequestBody UserManagement loginUser) {
		final Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginUser.getUserName(), loginUser.getUserPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		final UserManagement user = userManagementService.getUserByUserName(loginUser.getUserName());
		final String token = jwtTokenUtil.generateToken(user);
		return new ResponseEntity<>(new KpiResponseWrapper(null, token), HttpStatus.OK);
	}

}
