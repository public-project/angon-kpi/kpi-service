/**
 * 
 */
package com.id.kpi.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.ExamScheduleController;
import com.id.kpi.entity.ExamSchedule;
import com.id.kpi.service.ExamScheduleService;

/**
 * @author aulia
 *
 */
@RestController
public class ExamScheduleControllerImpl extends BaseController implements ExamScheduleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExamScheduleControllerImpl.class);

	@Autowired
	private ExamScheduleService examScheduleService;

	@Override
	@PostMapping(value = "exam/schedule")
	public ResponseEntity<KpiResponseWrapper> doSaveExamSchedule(@RequestBody ExamSchedule examSchedule)
			throws KpiException {
		try {
			ExamSchedule responseBody = examScheduleService.doSaveExamSchedule(examSchedule);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error on create exam schedule", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "exam/schedule/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateExamSchedule(@PathVariable(value = "id") Long id,
			@RequestBody ExamSchedule examSchedule) throws KpiException {
		try {
			ExamSchedule existingExamSchedule = examScheduleService.getExamSchedule(id);
			if (existingExamSchedule != null) {
				ExamSchedule responseBody = examScheduleService.doSaveExamSchedule(examSchedule);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.EXAM_SCHEDULE_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on update exam schedule", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "exam/schedule/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteExamSchedule(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			ExamSchedule existingExamSchedule = examScheduleService.getExamSchedule(id);
			if (existingExamSchedule != null) {
				examScheduleService.doDeleteExamSchedule(id);
				return new ResponseEntity<>(new KpiResponseWrapper(), HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.EXAM_SCHEDULE_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on delete exam schedule", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "exam/schedule")
	public ResponseEntity<KpiResponseWrapper> getAllExamSchedule(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "keyword", required = false) String keyword)
			throws KpiException {
		try {
			Page<ExamSchedule> responseBody = examScheduleService.getAllExamSchedule(page, size, keyword);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.EXAM_SCHEDULE_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get all exam schedule", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "exam/schedule/{id}")
	public ResponseEntity<KpiResponseWrapper> getExamSchedule(@PathVariable(value = "id") Long id) throws KpiException {
		try {
			ExamSchedule responseBody = examScheduleService.getExamSchedule(id);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.EXAM_SCHEDULE_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get exam schedule", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
