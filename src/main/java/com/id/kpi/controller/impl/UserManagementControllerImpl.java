/**
 * 
 */
package com.id.kpi.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.UserManagementController;
import com.id.kpi.entity.UserManagement;
import com.id.kpi.service.UserService;

/**
 * @author aulia
 *
 */
@RestController
public class UserManagementControllerImpl extends BaseController implements UserManagementController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementControllerImpl.class);

	@Autowired
	private UserService userManagementService;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	@Override
	@PostMapping(value = "user")
	public ResponseEntity<KpiResponseWrapper> doSaveUser(@RequestBody UserManagement user) throws KpiException {
		try {
			UserManagement saveEntity = user;
			saveEntity.setUserPassword(bcryptEncoder.encode(user.getUserPassword()));
			UserManagement responseBody = userManagementService.doSaveUser(saveEntity);
			return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.error("error on save user", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "user/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateUser(@PathVariable(value = "id") Long id,
			@RequestBody UserManagement user) throws KpiException {
		try {
			UserManagement updateUser = userManagementService.getUser(id);
			if (updateUser != null) {
				UserManagement saveEntity = user;
				saveEntity.setUserPassword(bcryptEncoder.encode(user.getUserPassword()));
				UserManagement responseBody = userManagementService.doSaveUser(saveEntity);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			}
			return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.USER_NOT_FOUND, null), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			LOGGER.error("error on update user", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "user/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteUser(@PathVariable(value = "id") Long id) throws KpiException {
		try {
			UserManagement updateUser = userManagementService.getUser(id);
			if (updateUser != null) {
				userManagementService.doDeleteUser(id);
				return new ResponseEntity<>(new KpiResponseWrapper(), HttpStatus.OK);
			}
			return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.USER_NOT_FOUND, null), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			LOGGER.error("error on delete user", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "user")
	public ResponseEntity<KpiResponseWrapper> getAllUser(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "keyword", required = false) String keyword)
			throws KpiException {
		try {
			Page<UserManagement> responseBody = userManagementService.getAllUser(page, size, checkIsNull(keyword));
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.USER_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get all user", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "user/{id}")
	public ResponseEntity<KpiResponseWrapper> getUser(@PathVariable(value = "id") Long id) throws KpiException {
		try {
			UserManagement responseBody = userManagementService.getUser(id);
			return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("error on get user", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
