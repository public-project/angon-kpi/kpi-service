/**
 * 
 */
package com.id.kpi.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.QuestionDetailController;
import com.id.kpi.entity.QuestionDetail;
import com.id.kpi.service.QuestionDetailService;

/**
 * @author aulia
 *
 */
@RestController
public class QuestionDetailControllerImpl extends BaseController implements QuestionDetailController {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuestionDetailControllerImpl.class);

	@Autowired
	private QuestionDetailService questionDetailService;

	@Override
	@PostMapping(value = "question/detail")
	public ResponseEntity<KpiResponseWrapper> doSaveQuestionDetail(@RequestBody QuestionDetail questionDetail)
			throws KpiException {
		try {
			QuestionDetail responseBody = questionDetailService.doSaveQuestionDetail(questionDetail);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error on create question detail", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "question/detail/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateQuestionDetail(@PathVariable(value = "id") Long id,
			@RequestBody QuestionDetail questionDetail) throws KpiException {
		try {
			QuestionDetail existingQuestionDetail = questionDetailService.getQuestionDetail(id);
			if (existingQuestionDetail != null) {
				QuestionDetail responseBody = questionDetailService.doSaveQuestionDetail(questionDetail);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_DETAIL_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on update question detail", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "question/detail/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteQuestionDetail(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			QuestionDetail existingQuestionDetail = questionDetailService.getQuestionDetail(id);
			if (existingQuestionDetail != null) {
				questionDetailService.doDeleteQuestionDetail(id);
				return new ResponseEntity<>(new KpiResponseWrapper(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_DETAIL_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on delete question detail", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "question/detail")
	public ResponseEntity<KpiResponseWrapper> getAllQuestionDetail(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size) throws KpiException {
		try {
			Page<QuestionDetail> responseBody = questionDetailService.getAllQuestionDetail(page, size);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_DETAIL_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get all question detail", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "question/detail/{id}")
	public ResponseEntity<KpiResponseWrapper> getQuestionDetail(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			QuestionDetail responseBody = questionDetailService.getQuestionDetail(id);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			}
			return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_DETAIL_NOT_FOUND, null),
					HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			LOGGER.error("error on question detail", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "question/detail/list/header")
	public ResponseEntity<KpiResponseWrapper> getQuestionDetailByHeader(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "headerId") Long headerId)
			throws KpiException {
		return null;
	}

}
