/**
 * 
 */
package com.id.kpi.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.ParticipantClassController;
import com.id.kpi.entity.ParticipantClass;
import com.id.kpi.service.ParticipantClassService;

/**
 * @author aulia
 *
 */
@RestController
public class ParticipantClassControllerImpl extends BaseController implements ParticipantClassController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParticipantClassControllerImpl.class);

	@Autowired
	private ParticipantClassService participantClassService;

	@Override
	@PostMapping(value = "participant/class")
	public ResponseEntity<KpiResponseWrapper> doSaveParticipantClass(@RequestBody ParticipantClass participantClass)
			throws KpiException {
		try {
			ParticipantClass responseBody = participantClassService.doSaveParticipantClass(participantClass);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error on create participant class", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "participant/class/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateParticipantClass(@PathVariable(value = "id") Long id,
			@RequestBody ParticipantClass participantClass) throws KpiException {
		try {
			ParticipantClass existingParticipantClass = participantClassService.getParticipantClass(id);
			if (existingParticipantClass != null) {
				ParticipantClass responseBody = participantClassService.doSaveParticipantClass(participantClass);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_CLASS_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on update participant class", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "participant/class/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteParticipantClass(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			ParticipantClass existingParticipantClass = participantClassService.getParticipantClass(id);
			if (existingParticipantClass != null) {
				participantClassService.doDeleteParticipantClass(id);
				return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_CLASS_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on delete participant class", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "participant/class")
	public ResponseEntity<KpiResponseWrapper> getAllParticipantClass(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "keyword", required = false) String keyword)
			throws KpiException {
		try {
			Page<ParticipantClass> responseBody = participantClassService.getAllParticipantClass(page, size,
					checkIsNull(keyword));
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_CLASS_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get all participant class", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "participant/class/{id}")
	public ResponseEntity<KpiResponseWrapper> getParticipantClass(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			ParticipantClass responseBody = participantClassService.getParticipantClass(id);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_CLASS_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get participant class", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
