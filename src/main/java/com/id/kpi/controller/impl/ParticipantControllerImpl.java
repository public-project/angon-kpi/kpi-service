/**
 * 
 */
package com.id.kpi.controller.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.ParticipantController;
import com.id.kpi.entity.Participant;
import com.id.kpi.service.ParticipantClassService;
import com.id.kpi.service.ParticipantService;
import com.id.kpi.util.ExcelUtil;
import com.id.kpi.util.KpiUtil;

/**
 * @author aulia
 *
 */
@RestController
public class ParticipantControllerImpl extends BaseController implements ParticipantController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParticipantControllerImpl.class);

	@Value("${spring.http.multipart.location}")
	private String storageLocation;

	@Value("${kpi.template.participant}")
	private String participantTemplate;

	@Autowired
	private ParticipantService participantService;

	@Autowired
	private ParticipantClassService participantClassService;

	@Autowired
	private ExcelUtil excelUtil;

	@Autowired
	private KpiUtil kpiUtil;

	@Override
	@PostMapping(value = "participant")
	public ResponseEntity<KpiResponseWrapper> doSaveParticipant(@RequestBody Participant participant)
			throws KpiException {
		try {
			if (participant.getParticipantClass().getId() == null) {
				participantClassService.doSaveParticipantClass(participant.getParticipantClass());
			}

			Participant responseBody = participantService.doSaveParticipant(participant);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error on create participant", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "participant/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateParticipant(@PathVariable(value = "id") Long id,
			@RequestBody Participant participant) throws KpiException {
		try {
			Participant existingParticipant = participantService.getParticipant(id);
			if (existingParticipant != null) {
				Participant responseBody = participantService.doSaveParticipant(participant);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on update participant", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "participant/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteParticipant(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			Participant existingParticipant = participantService.getParticipant(id);
			if (existingParticipant != null) {
				participantService.doDeleteParticipant(id);
				return new ResponseEntity<>(new KpiResponseWrapper(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on delete participant", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "participant")
	public ResponseEntity<KpiResponseWrapper> getAllParticipant(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "keyword", required = false) String keyword)
			throws KpiException {
		try {
			Page<Participant> responseBody = participantService.getAllParticipant(page, size, checkIsNull(keyword));
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get all participant", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "participant/{id}")
	public ResponseEntity<KpiResponseWrapper> getParticipant(@PathVariable(value = "id") Long id) throws KpiException {
		try {
			Participant responseBody = participantService.getParticipant(id);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.PARTICIPANT_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get participant", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PostMapping(value = "participant/upload")
	public ResponseEntity<KpiResponseWrapper> doUploadParticipant(
			@RequestPart(value = "file") MultipartFile uploadedFile) {
		try {
			if (kpiUtil.isExcelFile(uploadedFile.getOriginalFilename())) {
				byte[] bytes = uploadedFile.getBytes();
				Path path = Paths.get(storageLocation + uploadedFile.getOriginalFilename());
				Files.write(path, bytes);
				String responseBody = excelUtil.processParticipantFile(path);
				if (StringUtils.equalsIgnoreCase(KpiConstant.SUCCESS, responseBody)) {
					return new ResponseEntity<>(new KpiResponseWrapper(responseBody, null), HttpStatus.ACCEPTED);
				} else {
					return new ResponseEntity<>(new KpiResponseWrapper(responseBody, null), HttpStatus.BAD_REQUEST);
				}
			}
			return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			LOGGER.error("error on upload participant", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "participant/download")
	public ResponseEntity<Resource> doDownloadTemplateParticipant() {
		try {
			Path filePath = Paths.get(new File(participantTemplate).getAbsolutePath());
			ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(filePath));

			if (resource.exists()) {
				HttpHeaders httpHeader = new HttpHeaders();
				httpHeader.setContentType(MediaType.parseMediaType(KpiConstant.MEDIA_TYPE_XLSX));
				httpHeader.set(KpiConstant.HEADER_CONTENT_DISPOSITION,
						KpiConstant.HEADER_ATTACHMENT + filePath.getFileName());

				return new ResponseEntity<>(resource, httpHeader, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			LOGGER.error("error on download template participant", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
