/**
 * 
 */
package com.id.kpi.controller.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.entity.Participant;

/**
 * @author aulia
 *
 */
@RestController
public class DummyController extends BaseController {

	String fileXls = "/home/aulia/Downloads/Template_Login_Peserta.xls";
	String fileXlsx = "/home/aulia/Downloads/Template_Login_Peserta.xlsx";

	/*
	 * @GetMapping(value = "jxl") public void doReadXlsWithJxl(@RequestParam(value =
	 * "file") String file) { try { FileInputStream fileStream = null; if
	 * (file.equalsIgnoreCase("xls")) { fileStream = new FileInputStream(fileXls); }
	 * else { fileStream = new FileInputStream(fileXlsx); }
	 * 
	 * Workbook workbook = Workbook.getWorkbook(fileStream); Sheet sheet =
	 * workbook.getSheet("Data login peserta");
	 * 
	 * List<Participant> listOfParticipant = new ArrayList<>(); for (int rows = 1;
	 * rows < sheet.getRows(); rows++) { Participant participant = new
	 * Participant(); for (int columns = 0; columns < sheet.getColumns(); columns++)
	 * { participant.setParticipantCode(sheet.getCell(0, rows).getContents());
	 * participant.setParticipantFullName(sheet.getCell(1, rows).getContents());
	 * participant.setParticipantClass(sheet.getCell(2, rows).getContents()); }
	 * listOfParticipant.add(participant); } System.out.println("size: " +
	 * listOfParticipant.size());
	 * 
	 * for (Participant participant : listOfParticipant) {
	 * System.out.println(participant.getParticipantFullName()); } } catch
	 * (Exception e) { e.printStackTrace(); } }
	 */

	@GetMapping(value = "poi")
	public void doReadXlsWithPoi(@RequestParam(value = "file") String file) {
		try {
			FileInputStream fileStream = null;
			String excelType = null;
			if (file.equalsIgnoreCase("xls")) {
				fileStream = new FileInputStream(fileXls);
				excelType = fileXls;
			} else {
				fileStream = new FileInputStream(fileXlsx);
				excelType = fileXlsx;
			}

			Workbook workbook = getWorkbook(fileStream, excelType);
			Sheet sheet = workbook.getSheet("Data login peserta");
			Iterator<Row> iterator = sheet.iterator();

			List<Participant> listOfParticipant = new ArrayList<>();
			while (iterator.hasNext()) {
				Participant participant = new Participant();
				Row currentRow = iterator.next();
				if (currentRow.getRowNum() > 0) {
					participant.setParticipantCode(currentRow.getCell(0).getStringCellValue());
					participant.setParticipantFullName(currentRow.getCell(1).getStringCellValue());
//					participant.setParticipantClass(currentRow.getCell(2).getStringCellValue());
					listOfParticipant.add(participant);
				}
			}
			System.out.println("size: " + listOfParticipant.size());

			for (Participant participant : listOfParticipant) {
				System.out.println(participant.getParticipantFullName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Workbook getWorkbook(FileInputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;

		if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("Incorrect file format");
		}

		return workbook;
	}
	
	@Cacheable(value = "words", key = "#names")
	@GetMapping(value = "read/{names}")
	public ResponseEntity getRead(@PathVariable("names") String names) {
		return new ResponseEntity<>("Say Hello World to " + names, HttpStatus.OK);
	}

}
