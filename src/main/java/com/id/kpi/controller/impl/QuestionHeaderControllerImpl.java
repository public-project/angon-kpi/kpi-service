/**
 * 
 */
package com.id.kpi.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.kpi.base.BaseController;
import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.constant.KpiConstant;
import com.id.kpi.controller.QuestionHeaderController;
import com.id.kpi.entity.QuestionHeader;
import com.id.kpi.service.QuestionHeaderService;

/**
 * @author aulia
 *
 */
@RestController
public class QuestionHeaderControllerImpl extends BaseController implements QuestionHeaderController {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuestionHeaderControllerImpl.class);

	@Autowired
	private QuestionHeaderService questionHeaderService;

	@Override
	@PostMapping(value = "question/header")
	public ResponseEntity<KpiResponseWrapper> doSaveQuestionHeader(@RequestBody QuestionHeader questionHeader)
			throws KpiException {
		try {
			QuestionHeader responseBody = questionHeaderService.doSaveQuestionHeader(questionHeader);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(null, null), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error on create question header", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@PutMapping(value = "question/header/{id}")
	public ResponseEntity<KpiResponseWrapper> doUpdateQuestionHeader(@PathVariable(value = "id") Long id,
			@RequestBody QuestionHeader questionHeader) throws KpiException {
		try {
			QuestionHeader existingQuestionHeader = questionHeaderService.getQuestionHeader(id);
			if (existingQuestionHeader != null) {
				QuestionHeader responseBody = questionHeaderService.doSaveQuestionHeader(questionHeader);
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_HEADER_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on update question header", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@DeleteMapping(value = "question/header/{id}")
	public ResponseEntity<KpiResponseWrapper> doDeleteQuestionHeader(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			QuestionHeader existingQuestionHeader = questionHeaderService.getQuestionHeader(id);
			if (existingQuestionHeader != null) {
				questionHeaderService.doDeleteQuestionHeader(id);
				return new ResponseEntity<>(new KpiResponseWrapper(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_HEADER_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on delete question header", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "question/header")
	public ResponseEntity<KpiResponseWrapper> getAllQuestionHeader(@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size, @RequestParam(value = "keyword", required = false) String keyword)
			throws KpiException {
		try {
			Page<QuestionHeader> responseBody = questionHeaderService.getAllQuestionHeader(page, size,
					checkIsNull(keyword));
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_HEADER_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get all question header", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@GetMapping(value = "question/header/{id}")
	public ResponseEntity<KpiResponseWrapper> getQuestionHeader(@PathVariable(value = "id") Long id)
			throws KpiException {
		try {
			QuestionHeader responseBody = questionHeaderService.getQuestionHeader(id);
			if (responseBody != null) {
				return new ResponseEntity<>(new KpiResponseWrapper(null, responseBody), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new KpiResponseWrapper(KpiConstant.QUESTION_HEADER_NOT_FOUND, null),
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("error on get question header", e);
			return new ResponseEntity<>(new KpiResponseWrapper(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
