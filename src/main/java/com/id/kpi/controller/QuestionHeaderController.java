/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.QuestionHeader;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "Question Header API")
public interface QuestionHeaderController {

	@ApiOperation(value = "Save Question Header")
	public ResponseEntity<KpiResponseWrapper> doSaveQuestionHeader(QuestionHeader questionHeader) throws KpiException;

	@ApiOperation(value = "Update Question Header")
	public ResponseEntity<KpiResponseWrapper> doUpdateQuestionHeader(Long id, QuestionHeader questionHeader)
			throws KpiException;

	@ApiOperation(value = "Delete Question Header")
	public ResponseEntity<KpiResponseWrapper> doDeleteQuestionHeader(Long id) throws KpiException;

	@ApiOperation(value = "Get All Question Header")
	public ResponseEntity<KpiResponseWrapper> getAllQuestionHeader(int page, int size, String keyword) throws KpiException;

	@ApiOperation(value = "Get Question Header")
	public ResponseEntity<KpiResponseWrapper> getQuestionHeader(Long id) throws KpiException;

}
