/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.ParticipantClass;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "Participant Class API")
public interface ParticipantClassController {

	@ApiOperation(value = "Save Participant Class")
	public ResponseEntity<KpiResponseWrapper> doSaveParticipantClass(ParticipantClass participantClass)
			throws KpiException;

	@ApiOperation(value = "Update Participant Class")
	public ResponseEntity<KpiResponseWrapper> doUpdateParticipantClass(Long id, ParticipantClass participantClass)
			throws KpiException;

	@ApiOperation(value = "Delete Participant Class")
	public ResponseEntity<KpiResponseWrapper> doDeleteParticipantClass(Long id) throws KpiException;

	@ApiOperation(value = "Get All Participant Class")
	public ResponseEntity<KpiResponseWrapper> getAllParticipantClass(int page, int size, String keyword)
			throws KpiException;

	@ApiOperation(value = "Get Participant Class")
	public ResponseEntity<KpiResponseWrapper> getParticipantClass(Long id) throws KpiException;

}
