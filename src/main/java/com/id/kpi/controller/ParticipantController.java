/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.Participant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "Participant API")
public interface ParticipantController {

	@ApiOperation(value = "Save Participant")
	public ResponseEntity<KpiResponseWrapper> doSaveParticipant(Participant participant) throws KpiException;

	@ApiOperation(value = "Update Participant")
	public ResponseEntity<KpiResponseWrapper> doUpdateParticipant(Long id, Participant participant) throws KpiException;

	@ApiOperation(value = "Delete Participant")
	public ResponseEntity<KpiResponseWrapper> doDeleteParticipant(Long id) throws KpiException;

	@ApiOperation(value = "Get All Participant")
	public ResponseEntity<KpiResponseWrapper> getAllParticipant(int page, int size, String keyword) throws KpiException;

	@ApiOperation(value = "Get Participant")
	public ResponseEntity<KpiResponseWrapper> getParticipant(Long id) throws KpiException;

	@ApiOperation(value = "Upload Participant Data")
	public ResponseEntity<KpiResponseWrapper> doUploadParticipant(MultipartFile uploadedFile);

	@ApiOperation(value = "Download Template Participant Data")
	public ResponseEntity<Resource> doDownloadTemplateParticipant();

}
