/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.UserManagement;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "Authentication API")
public interface AuthenticateController {

	@ApiOperation(value = "Sign In")
	public ResponseEntity<KpiResponseWrapper> doSignIn(UserManagement user);

}
