/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.UserRole;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "User Role API")
public interface UserRoleController {

	@ApiOperation(value = "Save User Role")
	public ResponseEntity<KpiResponseWrapper> doSaveUserRole(UserRole role) throws KpiException;

	@ApiOperation(value = "Update User Role")
	public ResponseEntity<KpiResponseWrapper> doUpdateUserRole(Long id, UserRole role) throws KpiException;

	@ApiOperation(value = "Delete User Role")
	public ResponseEntity<KpiResponseWrapper> doDeleteUserRole(Long id) throws KpiException;

	@ApiOperation(value = "Get All User Role")
	public ResponseEntity<KpiResponseWrapper> getAllUserRole(int page, int size, String keyword) throws KpiException;

	@ApiOperation(value = "Get User Role")
	public ResponseEntity<KpiResponseWrapper> getUserRole(Long id) throws KpiException;
}
