/**
 * 
 */
package com.id.kpi.controller;

import org.springframework.http.ResponseEntity;

import com.id.kpi.base.exception.KpiException;
import com.id.kpi.base.wrapper.KpiResponseWrapper;
import com.id.kpi.entity.UserManagement;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author aulia
 *
 */
@Api(tags = "User Management API")
public interface UserManagementController {

	@ApiOperation(value = "Save User")
	public ResponseEntity<KpiResponseWrapper> doSaveUser(UserManagement user) throws KpiException;

	@ApiOperation(value = "Update User")
	public ResponseEntity<KpiResponseWrapper> doUpdateUser(Long id, UserManagement user) throws KpiException;

	@ApiOperation(value = "Delete User")
	public ResponseEntity<KpiResponseWrapper> doDeleteUser(Long id) throws KpiException;

	@ApiOperation(value = "Get All User")
	public ResponseEntity<KpiResponseWrapper> getAllUser(int page, int size, String keyword) throws KpiException;

	@ApiOperation(value = "Get User")
	public ResponseEntity<KpiResponseWrapper> getUser(Long id) throws KpiException;

}
