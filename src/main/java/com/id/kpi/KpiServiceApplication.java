package com.id.kpi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableAutoConfiguration
@EnableCaching
@EntityScan("com.id.kpi.entity")
@EnableJpaRepositories("com.id.kpi.repository")
@ComponentScan({ "com.id.kpi.service", "com.id.kpi.controller", "com.id.kpi.cryptograph", "com.id.kpi.util", "com.id.kpi.config" })
public class KpiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KpiServiceApplication.class, args);
	}
}
