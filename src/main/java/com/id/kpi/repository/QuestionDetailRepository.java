/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.QuestionDetail;

/**
 * @author aulia
 *
 */
@Repository
public interface QuestionDetailRepository extends JpaRepository<QuestionDetail, Long> {

}
