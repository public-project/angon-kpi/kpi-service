/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.QuestionHeader;

/**
 * @author aulia
 *
 */
@Repository
public interface QuestionHeaderRepository extends JpaRepository<QuestionHeader, Long> {
	
	@Query(value = "SELECT e FROM QuestionHeader e WHERE ((:questionCode IS NULL) OR (e.questionCode LIKE CONCAT('%',:questionCode,'%')))")
	public Page<QuestionHeader> findByQuestionCodeContaining(Pageable pageable, @Param("questionCode") String questionCode);
	
}
