/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.ParticipantClass;

/**
 * @author aulia
 *
 */
@Repository
public interface ParticipantClassRepository extends JpaRepository<ParticipantClass, Long> {

	@Query(value="SELECT e FROM ParticipantClass e WHERE (:className IS NULL OR e.className LIKE CONCAT('%', :className, '%'))")
	public Page<ParticipantClass> findByClassNameContaining(Pageable pageable, @Param("className") String className);

}
