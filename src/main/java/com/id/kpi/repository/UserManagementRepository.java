/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.UserManagement;

/**
 * @author aulia
 *
 */
@Repository
public interface UserManagementRepository extends JpaRepository<UserManagement, Long> {

	public UserManagement findByUserName(String userName);

	@Query(value = "SELECT e FROM UserManagement e WHERE ((:userName IS NULL) OR (e.userName LIKE CONCAT('%', :userName, '%')))")
	public Page<UserManagement> findByUserNameContaining(Pageable pageable, @Param("userName") String userName);

}
