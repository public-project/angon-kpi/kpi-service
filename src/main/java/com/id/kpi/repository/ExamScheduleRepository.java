/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.ExamSchedule;

/**
 * @author aulia
 *
 */
@Repository
public interface ExamScheduleRepository extends JpaRepository<ExamSchedule, Long> {

}
