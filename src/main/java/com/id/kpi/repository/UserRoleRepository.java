/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.UserRole;

/**
 * @author aulia
 *
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

	@Query(value = "SELECT e FROM UserRole e WHERE ((:roleName IS NULL) OR (e.roleName LIKE CONCAT('%', :roleName, '%')))")
	public Page<UserRole> findByRoleNameContaining(Pageable pageable, @Param("roleName") String roleName);

}
