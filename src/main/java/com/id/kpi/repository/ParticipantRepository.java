/**
 * 
 */
package com.id.kpi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.id.kpi.entity.Participant;

/**
 * @author aulia
 *
 */
@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

	@Query(value = "SELECT e FROM Participant e WHERE ((:participantFullName IS NULL) OR (e.participantFullName LIKE CONCAT('%',:participantFullName,'%')))")
	public Page<Participant> findByParticipantFullNameContaining(Pageable pageable, @Param("participantFullName") String participantFullName);

}
