/**
 * 
 */
package com.id.kpi.cryptograph;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.stereotype.Component;

/**
 * @author aulia
 *
 */
@Component
public class KpiServiceCryptograph {

	private static final StrongPasswordEncryptor PASSWORD_ENCRYPTOR = new StrongPasswordEncryptor();

	public String encryptPassword(String message) {
		return PASSWORD_ENCRYPTOR.encryptPassword(message);
	}

	public boolean checkPassword(String plainPassword, String encryptedPassword) {
		if (PASSWORD_ENCRYPTOR.checkPassword(plainPassword, encryptedPassword)) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

}
